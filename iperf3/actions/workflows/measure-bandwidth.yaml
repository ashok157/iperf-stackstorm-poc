version: 1.0
description: A sequential workflow that creates an experiment, runs an Iperf 3 command on server and client nodes, and terminates both nodes. It allows customization of experiment details, protocol selection, bandwidth and reverse configuration.
input:
  - name
  - profile
  - proj
  - bindings
  - wait_for_status
  - username
  - private_key
  - protocols
  - bandwidth
  - reverse

vars:
  - server_exp_addr: null
  - client_exp_addr: null
  - host_server: null
  - host_client: null
  - install_cmd: "sudo apt update ; sudo apt -y install iperf3"
  - iperf3_version_cmd: "iperf3 --version | head -1 | awk '{print $2}'"
  - result: <% list() %>
  - iperf3_version: ""

output:
  - result: <% ctx(result) %>
  - iperf3_version: <% ctx(iperf3_version) %>

tasks:
  create_experiment:
    action: emulab.experiment.create name=<% ctx().name %> profile=<% ctx().profile %> proj=<% ctx().proj %> bindings=<% ctx().bindings %> wait_for_status=<% ctx().wait_for_status %>
    next:
      - when: <% succeeded() %>
        publish:
          - server_exp_addr: <% result().result.manifest_facts['urn:publicid:IDN+emulab.net+authority+cm'].nodes['node-0'].links['lan-0'].address %>
          - client_exp_addr: <% result().result.manifest_facts['urn:publicid:IDN+emulab.net+authority+cm'].nodes['node-1'].links['lan-0'].address %>
          - host_server: <% result().result.status.aggregate_status['urn:publicid:IDN+emulab.net+authority+cm'].nodes['node-0'].ipv4 %>
          - host_client: <% result().result.status.aggregate_status['urn:publicid:IDN+emulab.net+authority+cm'].nodes['node-1'].ipv4 %>
        do: install_iperf3
      - when: <% failed() %>
        publish: result=<% result() %>
  install_iperf3:
    action: core.remote cmd=<% ctx().install_cmd %> hosts=<% ctx().host_server+','+ctx().host_client %> username=<% ctx().username %> private_key='<% ctx().private_key %>'
    next:
      - when: <% succeeded() %>
        do: get_iperf3_version
      - when: <% failed() %>
        publish: result=<% result() %>
  get_iperf3_version:
    action: core.remote cmd=<% ctx().iperf3_version_cmd %> hosts=<% ctx().host_server %> username=<% ctx().username %> private_key='<% ctx().private_key %>'
    next:
      - when: <% succeeded() %>
        do: run_subworkflow
        publish: iperf3_version=<% result()[ctx().host_server]["stdout"] %>
      - when: <% failed() %>
        publish: result=<% result() %>
  run_subworkflow:
    with:
      items: <% ctx(protocols) %>
      concurrency: 1
    action: iperf3.iperf3-core host_server=<% ctx(host_server) %> host_client=<% ctx(host_client) %> server_exp_addr=<% ctx(server_exp_addr) %> client_exp_addr=<% ctx(client_exp_addr) %> protocol=<% item() %> bandwidth=<% ctx(bandwidth) %> reverse=<% ctx(reverse) %> username=<% ctx().username %> private_key='<% ctx().private_key %>'
    next:
      - when: <% succeeded() %>
        publish: result=<% ctx("result") + [result().output.result] %>
        do: terminate_experiment
      - when: <% failed() %>
        publish: result=<% result() %>
        do: terminate_experiment
  terminate_experiment:
    action: emulab.experiment.terminate name=<% ctx().name %> proj=<% ctx().proj %>
    next:
      - when: <% succeeded() %>
      - when: <% failed() %>
        publish: result=<% result() %>
