# iperf3-stackstorm-poc

## Purpose of the Workflow:
The Iperf3 Bandwidth Measurement Workflow is designed to enable network administrators and DevOps teams to measure network bandwidth and performance between two endpoints. This workflow simplifies the process of configuring and executing Iperf3, providing valuable insights into network performance.

## Target Audience:
This documentation is intended for network administrators, DevOps engineers, and anyone responsible for network monitoring and performance testing. Users should have a basic understanding of networking concepts and be familiar with the StackStorm platform.

## Prerequisites:
Before using this workflow, make sure you have the following prerequisites in place:

StackStorm installed and configured in your environment.
Docker installed on the host where StackStorm is running (if you plan to use Docker containers for Iperf3).

## How to run:
After that, access the StackStorm Web UI, you must see iperf3 module under actions tab like below:

![Alt text](stackstorm_web_ui.png)

## Usage:
Running the Iperf3 Workflow

To initiate the Iperf3 Bandwidth Measurement Workflow from the StackStorm UI, follow these steps:

Navigate to the "Iperf3" action and expand it to reveal the available actions. Locate "iperf3.measure-bandwidth."
Click on "iperf3.measure-bandwidth" to access the workflow input form.
Provide the required input values for the workflow. You can configure the following parameters:

bindings: Specify the bindings using a JSON object, such as {"nodes": 2, "numberLans": 1}.
name:: Enter a name for the experiment, e.g., "test-iperf3."
profile: Define the profile for the experiment, e.g., "emulab-ops,dijon-git."
proj: Specify the project name, e.g., "PowderSandbox."
username: Enter your username, e.g., "abc."
protocols: Specify the network protocols to use. Allowed values are TCP or UDP.  By default it is TCP only. e.g., "TCP,UDP."
wait_for_status:: Set to "true" if you want to wait for the status of the experiement before proceeding.
reverse: Set to "true" if you want to run the iperf3 test in reverse mode. This is optional

Once you've provided the required input values, click "Run" to start the Iperf3 bandwidth measurement. This allows you to easily configure and initiate bandwidth measurement tests using the provided parameters.

Here is the sample run details for this:

![Alt text](iperf3_measure-bandwidth.png)
![Alt text](iperf3_measure-bandwidth.png)

To validate this execution, navigate to the "History" tab section. Here, you can expand the action to access comprehensive details of the run. This includes information such as input parameters, execution status, and any output or results generated during the process.

![Alt text](iperf3_output.png)

## Troubleshooting:
For troubleshooting purposes, if you encounter any errors during the execution of this action, please check the log located to the right of the output in the StackStorm web UI's history section. If the issue persists and you require further assistance, do not hesitate to reach out to the members of the Flux Research Group for expert support and guidance.